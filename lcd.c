/*
 * lcd.c
 *
 *  Created on: 11 de dic. de 2016
 *      Author: Roi
 */


// Based on the work by DFRobot

#include "stm32f1xx_hal.h"
#include "lcd.h"
#include "stdlib.h"
#include "stdio.h"

/** LCD properties */
#define	LCD_I2C_ADDRESS	0x7E
#define LCD_COLUMNS 16
#define LCD_ROWS 2

// commands
#define LCD_CLEARDISPLAY 0x01
#define LCD_RETURNHOME 0x02
#define LCD_ENTRYMODESET 0x04
#define LCD_DISPLAYCONTROL 0x08
#define LCD_CURSORSHIFT 0x10
#define LCD_FUNCTIONSET 0x20
#define LCD_SETCGRAMADDR 0x40
#define LCD_SETDDRAMADDR 0x80

// flags for display entry mode
#define LCD_ENTRYRIGHT 0x00
#define LCD_ENTRYLEFT 0x02
#define LCD_ENTRYSHIFTINCREMENT 0x01
#define LCD_ENTRYSHIFTDECREMENT 0x00

// flags for display on/off control
#define LCD_DISPLAYON 0x04
#define LCD_DISPLAYOFF 0x00
#define LCD_CURSORON 0x02
#define LCD_CURSOROFF 0x00
#define LCD_BLINKON 0x01
#define LCD_BLINKOFF 0x00

// flags for display/cursor shift
#define LCD_DISPLAYMOVE 0x08
#define LCD_CURSORMOVE 0x00
#define LCD_MOVERIGHT 0x04
#define LCD_MOVELEFT 0x00

// flags for function set
#define LCD_8BITMODE 0x10
#define LCD_4BITMODE 0x00
#define LCD_2LINE 0x08
#define LCD_1LINE 0x00
#define LCD_5x10DOTS 0x04
#define LCD_5x8DOTS 0x00

// flags for backlight control
#define LCD_BACKLIGHT 0x08
#define LCD_NOBACKLIGHT 0x00

#define En 0x04  // Enable bit
#define Rw 0x02  // Read/Write bit
#define Rs 0x01 // Register select bit

void Send(uint8_t value, uint8_t mode);
void Write4bits(uint8_t value);
void ExpanderWrite(uint8_t _data);
void PulseEnable(uint8_t _data);

uint8_t _backlightval = LCD_NOBACKLIGHT;
uint8_t _displayfunction = 0;
uint8_t _displaymode = 0;
uint8_t _displaycontrol = 0;

static I2C_HandleTypeDef * i2c1_p;


void LcdInit(I2C_HandleTypeDef * i2c1)
{
	i2c1_p = i2c1;

	_displayfunction = LCD_4BITMODE | LCD_2LINE | LCD_5x8DOTS;

	HAL_Delay(1);

	// Now we pull both RS and R/W low to begin commands
	ExpanderWrite(_backlightval);	// reset expanderand turn backlight off (Bit 8 =1)
	HAL_Delay(1);

  	//put the LCD into 4 bit mode
	// this is according to the hitachi HD44780 datasheet
	// figure 24, pg 46

	// we start in 8bit mode, try to set 4 bit mode
	Write4bits(0x03 << 4);
	HAL_Delay(5); // wait min 4.1ms

	// second try
	Write4bits(0x03 << 4);
	HAL_Delay(5); // wait min 4.1ms

	// third go!
	Write4bits(0x03 << 4);
	HAL_Delay(1);

	// finally, set to 4-bit interface
	Write4bits(0x02 << 4);


	// set # lines, font size, etc.
	LcdCommand(LCD_FUNCTIONSET | _displayfunction);

	// turn the display on with no cursor or blinking default
	_displaycontrol = LCD_DISPLAYON | LCD_CURSOROFF| LCD_BLINKOFF;
	LcdDisplay();

	// clear it off
	LcdClear();

	// Initialize to default text direction (for roman languages)
	_displaymode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;

	// set the entry mode
	LcdCommand(LCD_ENTRYMODESET | _displaymode);

	LcdHome();
}

/********** high level commands, for the user! */
void LcdClear(void)
{
	LcdCommand(LCD_CLEARDISPLAY);// clear display, set cursor position to zero
	HAL_Delay(2);  // this command takes a long time!
}

void LcdHome(void)
{
	LcdCommand(LCD_RETURNHOME);  // set cursor position to zero
	HAL_Delay(2);  // this command takes a long time!
}

void LcdSetCursor(uint8_t col, uint8_t row)
{
	int row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };
	if ( row > LCD_ROWS ) {
		row = LCD_ROWS-1;    // we count rows starting w/0
	}
	LcdCommand(LCD_SETDDRAMADDR | (col + row_offsets[row]));
}

// Turn the display on/off (quickly)
void LcdNoDisplay(void)
{
	_displaycontrol &= ~LCD_DISPLAYON;
	LcdCommand(LCD_DISPLAYCONTROL | _displaycontrol);
}
void LcdDisplay(void)
{
	_displaycontrol |= LCD_DISPLAYON;
	LcdCommand(LCD_DISPLAYCONTROL | _displaycontrol);
}

// Turns the underline cursor on/off
void LcdNoCursor(void)
{
	_displaycontrol &= ~LCD_CURSORON;
	LcdCommand(LCD_DISPLAYCONTROL | _displaycontrol);
}
void LcdCursor(void)
{
	_displaycontrol |= LCD_CURSORON;
	LcdCommand(LCD_DISPLAYCONTROL | _displaycontrol);
}

// Turn on and off the blinking cursor
void LcdNoBlink(void)
{
	_displaycontrol &= ~LCD_BLINKON;
	LcdCommand(LCD_DISPLAYCONTROL | _displaycontrol);
}
void LcdBlink(void)
{
	_displaycontrol |= LCD_BLINKON;
	LcdCommand(LCD_DISPLAYCONTROL | _displaycontrol);
}

// These commands scroll the display without changing the RAM
void LcdScrollDisplayLeft(void)
{
	LcdCommand(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
}
void LcdScrollDisplayRight(void)
{
	LcdCommand(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
}

// This is for text that flows Left to Right
void LcdLeftToRight(void)
{
	_displaymode |= LCD_ENTRYLEFT;
	LcdCommand(LCD_ENTRYMODESET | _displaymode);
}

// This is for text that flows Right to Left
void LcdRightToLeft(void)
{
	_displaymode &= ~LCD_ENTRYLEFT;
	LcdCommand(LCD_ENTRYMODESET | _displaymode);
}

// This will 'right justify' text from the cursor
void LcdAutoscroll(void)
{
	_displaymode |= LCD_ENTRYSHIFTINCREMENT;
	LcdCommand(LCD_ENTRYMODESET | _displaymode);
}

// This will 'left justify' text from the cursor
void LcdNoAutoscroll(void)
{
	_displaymode &= ~LCD_ENTRYSHIFTINCREMENT;
	LcdCommand(LCD_ENTRYMODESET | _displaymode);
}

// Turn the (optional) backlight off/on
void LcdNoBacklight(void)
{
	_backlightval=LCD_NOBACKLIGHT;
	ExpanderWrite(0);
}

void LcdBacklight(void)
{
	_backlightval=LCD_BACKLIGHT;
	ExpanderWrite(0);
}



/*********** mid level commands, for Sending data/cmds */

void LcdCommand(uint8_t value)
{
	Send(value, 0);
}


/************ low level data pushing commands **********/

// write either command or data
void Send(uint8_t value, uint8_t mode)
{
	uint8_t highnib=value&0xf0;
	uint8_t lownib=(value<<4)&0xf0;
       Write4bits((highnib)|mode);
	Write4bits((lownib)|mode);
}

void Write4bits(uint8_t value)
{
	ExpanderWrite(value);
	PulseEnable(value);
}

void ExpanderWrite(uint8_t _data)
{
	_data |= _backlightval;
	HAL_I2C_Master_Transmit(i2c1_p, LCD_I2C_ADDRESS, &_data, 1, 10000);
}

void PulseEnable(uint8_t _data)
{
	ExpanderWrite(_data | En);	// En high
	HAL_Delay(1);		// enable pulse must be >450ns

	ExpanderWrite(_data & ~En);	// En low
	HAL_Delay(50);		// commands need > 37us to settle
}

void LcdWrite(const char* c)
{
	uint8_t i = 0;
	while (c[i] != 0)
	{
		Send(c[i], Rs);
		i ++;
	}
}

void LcdWriteChar(char caracter)
{
	Send(caracter, Rs);
}

void LcdWriteUNumber(uint32_t number)
{
	char num_string[11];
	itoa(number, num_string, 10);
	LcdWrite(num_string);
}

void LcdWriteXNumber(int32_t number)
{
	char num_string[12];
	itoa(number, num_string, 10);
	LcdWrite(num_string);
}
