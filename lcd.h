/*
 * lcd.h
 *
 *  Created on: 11 de dic. de 2016
 *      Author: Roi
 */

#ifndef LCD_H_
#define LCD_H_

void LcdClear(void);
void LcdHome(void);
void LcdNoDisplay(void);
void LcdDisplay(void);
void LcdNoBlink(void);
void LcdBlink(void);
void LcdNoCursor(void);
void LcdCursor(void);
void LcdScrollDisplayLeft(void);
void LcdScrollDisplayRight(void);
void LcdLeftToRight(void);
void LcdRightToLeft(void);
void LcdNoBacklight(void);
void LcdBacklight(void);
void LcdAutoscroll(void);
void LcdNoAutoscroll(void);
void LcdSetCursor(uint8_t col, uint8_t row);
void LcdWrite(const char* c);
void LcdCommand(uint8_t value);
void LcdInit(I2C_HandleTypeDef * i2c1);
void LcdWriteChar(char caracter);
void LcdWriteUNumber(uint32_t number);
void LcdWriteXNumber(int32_t number);

#endif /* LCD_H_ */
